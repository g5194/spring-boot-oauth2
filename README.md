# Spring Boot oAuth2

Setting up Spring Boot oAuth2 server sample application. Usage details will be added through postman.

## Case Study
    * Setup the oauth2 server with username and password 
## Sequence flow

## Pre-requisites
    * Java 1.8 
    * Maven 
    * Postman for understanding the API flow 
## Getting started
Clean and build

Step1:
Create Spring Boot application with web and rest API's

    mvn clean install 
Run application

    mvn spring-boot:run 

* mvn clean install
    * mvn spring-boot:run
## Add your files



## Name
Spring Boot oAuth2 Server

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## To be explored later
* @PostAuthorize
* @PreAuthorize
* @Secured
* @EnableGlobalMethodSecurity(securedEnabled = true)


[//]: # (## Roadmap)

[//]: # (If you have ideas for releases in the future, it is a good idea to list them in the README.)

[//]: # ()

## Authors and acknowledgment
    -Damodar Naidu M 
      Senior Software Engineer

## License
For open source projects, Spring-Boot oAuth sample application.
