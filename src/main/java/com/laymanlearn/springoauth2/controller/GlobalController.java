package com.laymanlearn.springoauth2.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GlobalController {

    @GetMapping("/global/message")
    public String getMessage(){
        return "Open API....";
    }
}
