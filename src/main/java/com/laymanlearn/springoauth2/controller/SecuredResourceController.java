package com.laymanlearn.springoauth2.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SecuredResourceController {

    @GetMapping("/secure/sample-key")
    public String getSecureKey(){
        return "Secure-1234";
    }
}
