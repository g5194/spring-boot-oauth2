package com.laymanlearn.springoauth2.security.resource;

import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Arrays;

@Slf4j
@Service(value = "userDetailsServiceImpl")
public class UserDetailsServiceImpl implements UserDetailsService {
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        log.info("****** Init verification with user name {}", username);
        return new User("testuser", "$2a$10$b2Vk4H898DGuTDCPLL4bdugmM9l7Z3KyA78WALiIEMja1OtnI2/Tm", Arrays.asList(new SimpleGrantedAuthority("ADMIN")));
    }
}
